package pl.edu.pwsztar.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ChessServiceImpl implements ChessService {

    @Override
    public boolean checkTypeAndMove(FigureMoveDto figureMoveDto) {
        boolean isPossible = false;
        FigureType figure = figureMoveDto.getType();
        if(FigureType.BISHOP==figure) {
            isPossible = validateMoveBishop(figureMoveDto);
        }
        return isPossible;
    }

    private boolean validateMoveBishop(FigureMoveDto figureMoveDto) {
        String[] startPos = figureMoveDto.getStart().split("_");
        String[] desPos = figureMoveDto.getDestination().split("_");
        List<String> positions = new ArrayList<>(Arrays.asList("a","b","c","d","e","f","g","h"));

        int currentRow = positions.indexOf(startPos[0]);
        int currentCol = Integer.parseInt(startPos[1]);
        int newRow = positions.indexOf(desPos[0]);
        int newCol = Integer.parseInt(desPos[1]);

        if(Math.abs(newRow-currentRow) != Math.abs(newCol-currentCol)){
            return false;
        }else{
            return true;
        }

    }
}
